interface AuthConfig {
  CLIENT_ID: string;
  CLIENT_DOMAIN: string;
  // callbackURL: string;
}


export const AUTH_CONFIG: AuthConfig = {
  CLIENT_ID: '[AUTH0_CLIENT_ID]',
  CLIENT_DOMAIN: '[AUTH0_CLIENT_DOMAIN]',
}
