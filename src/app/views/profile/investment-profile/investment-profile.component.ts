import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { IOption } from 'ng-select';
declare var $;
@Component({
  selector: 'app-investment-profile',
  templateUrl: './investment-profile.component.html',
  styleUrls: ['./investment-profile.component.scss',
    '../../../../scss/vendors/ng-select/ng-select.scss',
    '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class InvestmentProfileComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.range_slider').ixRangeSlider();

    //Event
    $('.range_slider').on('ixRangeSlider:change', function (e) {
      console.log('values:' + e.values);
    });
  }

  public countries: Array<IOption> = [
    { label: 'A Param', value: 'BE' },
    { label: 'B Param', value: 'LU' },
    { label: 'C Param', value: 'NL' }
  ];
  public selectedCountries: Array<string> = ['BE', 'NL'];
}
