import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalProfileComponent } from './personal-profile/personal-profile.component';
import { InvestmentProfileComponent } from './investment-profile/investment-profile.component';


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'profile'
        },
        children: [
            {
                path: 'Add_profile',
                component: PersonalProfileComponent,
                data: {
                    title: 'Create'
                }
            },
            {
                path: 'Add_investment',
                component: InvestmentProfileComponent
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRouting { }
