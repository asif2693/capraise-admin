import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonalProfileComponent } from './personal-profile/personal-profile.component';
import { InvestmentProfileComponent } from './investment-profile/investment-profile.component';
import { ProfileRouting } from './profile.routing.module';
import { TextMaskModule } from 'angular2-text-mask';
// Timepicker
import { TimepickerModule } from 'ngx-bootstrap';
// Datepicker
import { BsDatepickerModule } from 'ngx-bootstrap';
// Ng2-select
import { SelectModule } from 'ng-select';
import {FormsModule,
  ReactiveFormsModule,} from '@angular/forms'

@NgModule({
  imports: [
    CommonModule,
    ProfileRouting,
    TextMaskModule,
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    SelectModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [PersonalProfileComponent, InvestmentProfileComponent]
})
export class ProfileModule { }
