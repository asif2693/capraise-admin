import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateDealParameterComponent } from './create-deal-parameter/create-deal-parameter.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CreateDealParameterComponent]
})
export class CreateDealParameterModule { }
