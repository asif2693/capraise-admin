import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {CreateDealParameterComponent} from '../create-deal-parameter/create-deal-parameter.component';
const CreateDealParameterRoutes: Routes = [
  {
    path: '',
    component: CreateDealParameterComponent,
    data: {
      title: 'Create Deal Parameter'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(CreateDealParameterRoutes)],
  exports: [RouterModule]
})
export class CreateDealParameterRoutingModule{ }
