import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RequestDealComponent} from './request-deal.component'
const routes: Routes = [
  {
    path: '',
    component: RequestDealComponent,
    data: {
      title: 'Request Deals'
    },
    children: [
      {
        path: 'Request_deals',
        component: RequestDealComponent,
        data: {
          title: 'Request'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestDealRoutingModule { }
