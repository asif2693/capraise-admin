import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestDealComponent } from './request-deal.component';

import { RequestDealRoutingModule } from './request-deal.routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule } from '@angular/forms';

//DataTable

import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { DataFilterPipe } from './datafilterpipe';

@NgModule({
  imports: [
    CommonModule,
    RequestDealRoutingModule,
    DataTableModule,
    FormsModule,
    HttpModule,
    ModalModule.forRoot(),
  ],
  declarations: [RequestDealComponent, DataFilterPipe]
})
export class RequestDealModule { }
