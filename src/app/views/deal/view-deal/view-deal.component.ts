import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-deal',
  templateUrl: './view-deal.component.html',
  styleUrls: ['./view-deal.component.scss']
})
export class ViewDealComponent implements OnInit {
  goCreate(){
    this.router.navigate(["deals/Add_deal"]);
  }
  item = [
    {
      name: "A Company",
      Amount: "200",
      Active: "yes",
      type: "Debt"
    },
    {
      name: "B Company",
      Amount: "5000",
      Active: "yes",
      type: "Credit"

    },
    {
      name: "C Company",
      Amount: "1000",
      Active: "no",
      type: "Credit"
    },

    {
      name: "D Company",
      Amount: "9000",
      Active: "yes",
      type: "Credit"
    },

  ]

  public data;
  public filterQuery = '';

  constructor(private http: Http, private router: Router) {
    http.get('data.json')
      .subscribe((data) => {
        setTimeout(() => {
          this.data = data.json();
        }, 2000);
      });
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }
  getStyle() {
    for (let i = 0; i <= this.item.length; i++) {
      if (this.item[i].Active == 'no') {
        return "red";
      } else if (this.item[i].Active == 'yes') {
        return "yellow";
      }
    }
  }
  ngOnInit() {
  }

}
