import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewDealComponent } from './view-deal.component'


const routes: Routes = [
    {
        path: '',
        component: ViewDealComponent,
        data: {
            title: 'View Deals'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ViewDealRoutingModule { }
