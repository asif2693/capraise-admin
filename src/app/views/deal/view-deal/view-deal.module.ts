import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Module

import { ViewDealRoutingModule } from './view-deal.routing.module';
//DataTable

import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';

import { DataFilterPipe } from './datafilterpipe';
//component
import { ViewDealComponent } from './view-deal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    ViewDealRoutingModule,
    DataTableModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [ViewDealComponent, DataFilterPipe]
})
export class ViewDealModule { }
