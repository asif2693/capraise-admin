import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import { DealComponent } from './deal.component';
import { CreateDealComponent } from './create-deal/create-deal.component';
import { ViewDealComponent } from './view-deal/view-deal.component';
import {RequestDealModule} from './request-deal/request-deal.module';
import {CreateRequestDealComponent} from './create-request-deal/create-request-deal.component';
import {CreateDealParameterComponent} from './create-deal-parameter/create-deal-parameter.component'
const DealRoutes: Routes = [
  {
    path: '',
    data: {
      title: 'Deals'
    },
    children: [
      {
        path: 'Add_deal',
        component: CreateDealComponent,
        data: {
          title: 'Create'
        }
      },
      {
        path: 'view',
        loadChildren: './view-deal/view-deal.module#ViewDealModule'
      },
      {
        path: 'request',
        loadChildren: './request-deal/request-deal.module#RequestDealModule'
      },
      {
        path: 'create-request-deal',
        component: CreateRequestDealComponent,
        data: {
          title: 'Create Request Deal'
        }
      },{
        path: 'create-deal-parameter',
        component: CreateDealParameterComponent,
        data: {
          title: 'Create Deal Parameter'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(DealRoutes)],
  exports: [RouterModule]
})
export class DealRoutingModule { }
