import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DealComponent } from '../../views/deal/deal.component';
import {
  Routes,
  RouterModule
} from '@angular/router';
import { DealRoutingModule } from './deal-routing.module';
import { CreateDealComponent } from './create-deal/create-deal.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AdvancedFormsComponent } from '../forms/advanced-forms/advanced-forms.component'
import { FormsModule } from '@angular/forms';

// Angular 2 Input Mask
import { TextMaskModule } from 'angular2-text-mask';

// Timepicker
import { TimepickerModule } from 'ngx-bootstrap';

// Datepicker
import { BsDatepickerModule } from 'ngx-bootstrap';

// Ng2-select
import { SelectModule } from 'ng-select';
import { CreateRequestDealComponent } from './create-request-deal/create-request-deal.component';
import {CreateDealParameterComponent} from './create-deal-parameter/create-deal-parameter.component';
// import {RequestDealComponent} from './request-deal/request-deal.component';
// dataTable


@NgModule({

  imports: [
    CommonModule,
    FormsModule,
    DealRoutingModule,
    TextMaskModule,
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    SelectModule,
    FormsModule,
  ],
  declarations: [DealComponent, CreateDealComponent, CreateRequestDealComponent, CreateDealParameterComponent]
})
export class DealModule { }
