import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateRequestDealComponent } from './create-request-deal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CreateRequestDealComponent]
})
export class CreateRequestDealModule { }
