import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {Auth0Service} from '../../service/auth0.service'

declare const gapi: any;
declare var window: any;
declare var FB: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  public auth2: any;
  constructor(private router: Router, private http: HttpClient,public auth:Auth0Service) {
    // auth.handleAuthentication()

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    window.fbAsyncInit = () => {
      console.log("fbasyncinit")

      FB.init({
        appId: '209907512998050',
        autoLogAppEvents: true,
        xfbml: true,
        version: 'v3.0'
      });
      FB.AppEvents.logPageView();

      FB.Event.subscribe('auth.statusChange', (response => {
        if (response.status === 'connected') {
          this.router.navigate(['/dashboard'])

          // use the response variable to get any information about the user and to see the tokens about the users session
        }

      }));

    }
  }

  googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '375947721726-ruuekf6nn7hdhomqogufh5pmejfs7g3q.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });
      this.attachSignin(document.getElementById('googleBtn'));
    });
  }

  public loginfb(){
    FB.login((response)=>{
      if (response.authResponse) {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) {
          console.log('Good to see you, ' + response.name + '.');
        });

      } else {
        console.log('User cancelled login or did not fully authorize.');
      }
    });
  }

  public attachSignin(element) {
    this.auth2.attachClickHandler(element, {},
      (googleUser) => {
        let profile = googleUser.getBasicProfile();
        //YOUR CODE HERE
        this.router.navigate(['/dashboard'])
      }, (error) => {
        alert(JSON.stringify(error, undefined, 2));
      });
  }

  ngAfterViewInit(){
    this.googleInit();
  }





  ngOnInit() {
    if (window.FB) {
      window.FB.XFBML.parse();
    }
  }







  login(){
    this.router.navigate(["/dashboard"]);
  }
}
